package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    CellType[][] labyrinth;
    Coordinate playerPos = new Coordinate(0, 0);

    public LabyrinthImpl() {

    }

    public static void main(String[] args) {
        LabyrinthImpl l = new LabyrinthImpl();
        l.loadLabyrinthFile("labyrinth1.txt");
        List<Direction> moves = l.possibleMoves();

        System.out.println(l.possibleMoves().size());
        RandomPlayer r = new RandomPlayer();
        System.out.println(r.nextMove(l));
        for (Direction possibleMove : l.possibleMoves()) {
            System.out.println(possibleMove);
        }
    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());
            setSize(width, height);
            for (int ww = 0; ww < width; ww++) {
                String line = sc.nextLine();
                for (int hh = 0; hh < height; hh++) {
                    switch (line.charAt(hh)) {
                        case 'W':
                            labyrinth[hh][ww] = CellType.WALL;
                            break;
                        case 'E':
                            labyrinth[hh][ww] = CellType.END;
                            break;
                        case 'S':
                            labyrinth[hh][ww] = CellType.START;
                            playerPos = new Coordinate(hh, ww);
                            break;
                        default:
                            labyrinth[hh][ww] = CellType.EMPTY;
                            break;
                    }
                }
            }

        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public int getWidth() {
        if (labyrinth == null) {
            return -1;
        }
        return labyrinth.length;
    }

    @Override
    public int getHeight() {
        if (labyrinth == null) {
            return -1;
        }
        return labyrinth[0].length;
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {
        if (c.getRow() > labyrinth.length - 1 || c.getRow() < 0 || c.getCol() > labyrinth[0].length - 1 || c.getCol() < 0) {
            throw new CellException(c, "hibás koordináta!");
        }
        return labyrinth[c.getCol()][c.getRow()];
    }

    @Override
    public void setSize(int width, int height) {
        labyrinth = new CellType[width][height];
        for (int i = 0; i < labyrinth.length; i++) {
            for (int j = 0; j < labyrinth[0].length; j++) {
                labyrinth[i][j] = CellType.EMPTY;
            }
        }
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        if (c.getCol() > labyrinth.length - 1 || c.getCol() < 0 || c.getRow() > labyrinth[0].length - 1 || c.getRow() < 0) {
            throw new CellException(c, "Nincs ilyen koordináta!");
        }
        if (type.equals(CellType.START)) {
            playerPos = c;
        }
        labyrinth[c.getCol()][c.getRow()] = type;

    }

    @Override
    public Coordinate getPlayerPosition() {
        return playerPos;
    }

    @Override
    public boolean hasPlayerFinished() {
        if (labyrinth[playerPos.getCol()][playerPos.getRow()].equals(CellType.END)) {
            return true;
        }
        return false;
    }

    @Override
    public List<Direction> possibleMoves() {
        List<Direction> directions = new ArrayList<>();

        if (labyrinth[playerPos.getRow() - 1][playerPos.getCol()] == CellType.EMPTY
                || labyrinth[playerPos.getRow() - 1][playerPos.getCol()] == CellType.END && playerPos.getRow() > 0) {
            directions.add(Direction.WEST);
        }
        if (labyrinth[playerPos.getCol() + 1][playerPos.getRow()] == CellType.EMPTY
                || labyrinth[playerPos.getCol() + 1][playerPos.getRow()] == CellType.END && playerPos.getCol() < labyrinth.length) {
            directions.add(Direction.EAST);
        }
        if (playerPos.getCol() > 0) {
            if (labyrinth[playerPos.getCol()][playerPos.getRow() - 1] == CellType.EMPTY
                    || labyrinth[playerPos.getCol()][playerPos.getRow() - 1] == CellType.END) {
                directions.add(Direction.NORTH);
            }
        }
        if (playerPos.getRow() < labyrinth[0].length) {
            if (labyrinth[playerPos.getCol()][playerPos.getRow() + 1] == CellType.EMPTY
                    || labyrinth[playerPos.getCol()][playerPos.getRow() + 1] == CellType.END) {
                directions.add(Direction.SOUTH);
            }
        }
        return directions;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {

        if (possibleMoves().contains(direction)) {
            Coordinate newPos = null;
            if (direction.equals(Direction.WEST) && playerPos.getCol() > 0) {
                newPos = new Coordinate(playerPos.getCol() - 1, playerPos.getRow());
            }
            if (direction.equals(Direction.EAST)) {
                newPos = new Coordinate(playerPos.getCol() + 1, playerPos.getRow());
            }
            if (direction.equals(Direction.NORTH)) {
                newPos = new Coordinate(playerPos.getCol(), playerPos.getRow() - 1);
            }
            if (direction.equals(Direction.SOUTH)) {
                newPos = new Coordinate(playerPos.getCol(), playerPos.getRow() + 1);
            }
            playerPos = newPos;
        } else {
            throw new InvalidMoveException();
        }
    }

}
